# Hell Mod by 56

## Effects
- 1/2 Nail Damage
- 2x incoming damage
- 1/3 max SOUL
- 1/2 SOUL collection
- no SOUL collection while your shade is alive
- Heal focus is nerfed to Deep Focus speed
- Deep focus multiplier increased by 1.3725
- Dream shield only costs 1 notch (bugged, shows up as 1 but still uses 3)

## Installation
Hell Mod is an API mod, so install the Hollow Knight API (by Seanpr, Firezen, and Wyza) and then place the HellMod.dll into the Mods folder


## Thanks
Thanks a lot to everyone who gave ideas and helped me code it. A few special mentions are listed below.
Code:
- Wyza
- Seanpr
- KDT
Ideas and Testing:
- Scenic
- Verulean
- Laphi